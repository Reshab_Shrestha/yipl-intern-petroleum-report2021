```bash
pip install -r requirements.txt

python manage.py migrate

python manage.py runserver
```

#Navigate
http://127.0.0.1:8000/countrysale/ to generate overall sale of each petroleum product by country in HTML.

http://127.0.0.1:8000/productsale/to generate average sale of each petroleum product for 2 years of interval in HTML.

http://127.0.0.1:8000/leastsale/ to generate year each petroleum product had the least sale in HTML.

#Tests
To run the test, cd into the directory where manage.py is 
```sh
(env)$ python manage.py test 
```