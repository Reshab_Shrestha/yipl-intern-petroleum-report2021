from django.db import models


# Create your models here.


class Product(models.Model):
    ProductName = models.CharField(max_length=100)

    def __str__(self):
        return str(self.ProductName)


class Country(models.Model):
    CountryName = models.CharField(max_length=100)

    def __str__(self):
        return str(self.CountryName)


class Report(models.Model):
    Year = models.CharField(max_length=50)
    Sale = models.IntegerField()
    ProductName = models.ForeignKey(Product, on_delete=models.CASCADE)
    CountryName = models.ForeignKey(Country, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {} {} {}'.format(self.ProductName, self.Year, self.Sale, self.CountryName)
