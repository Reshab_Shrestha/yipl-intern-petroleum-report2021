from django.db.models import Avg, Sum


import requests
from django.shortcuts import render

from Report.models import Product, Country, Report


# Create your views here.
def data():
    response = requests.get(
        'https://raw.githubusercontent.com/younginnovations/internship-challenges/master/programming/petroleum'
        '-report/data.json')
    d = response.json()
    for i in d:
        year = i['year']
        name = i['petroleum_product']
        sale = i['sale']
        country = i['country']
        if not Report.objects.filter(Year=year, Sale=sale, CountryName__CountryName__contains=country,
                                     ProductName__ProductName__contains=name).exists():
            try:
                obj1 = Product.objects.get(ProductName=name)
            except Product.DoesNotExist:
                obj1 = Product.objects.create(ProductName=name)
                obj1.save()
            try:
                obj2 = Country.objects.get(CountryName=country)
            except Country.DoesNotExist:
                obj2 = Country.objects.create(CountryName=country)
                obj2.save()
            report = Report.objects.create(Year=year, Sale=sale, ProductName=obj1, CountryName=obj2)
            report.save()


def sale_country(request):
    data()
    result = []
    for i in Country.objects.all():
        a = Report.objects.filter(CountryName=i)
        for j in Product.objects.all():
            b = a.filter(ProductName=j)
            r1 = {
                'Countryname': i,
                'Productname': j,
                'AvgSale': b.exclude(Sale=0).aggregate(Avg=Avg('Sale')),
                'Total': b.exclude(Sale=0).aggregate(Sum=Sum('Sale')),
            }
            result.append(r1)

    return render(request, 'CountrySale.html', {'context': result})


def sale_product(request):
    data()
    result = []
    for i in Product.objects.all():
        a = Report.objects.filter(ProductName=i)
        b = a.filter(Year__gte=2013, Year__lte=2014)
        c = a.filter(Year__gte=2011, Year__lte=2012)
        d = a.filter(Year__gte=2009, Year__lte=2010)
        e = a.filter(Year__gte=2007, Year__lte=2008)
        r1 = {
            'Name': i,
            'Year': b[4].Year + "-" + b[0].Year,
            'Result': b.exclude(Sale=0).aggregate(Avg=Avg('Sale'))
        }
        r2 = {
            'Name': i,
            'Year': c[4].Year + "-" + c[0].Year,
            'Result': c.exclude(Sale=0).aggregate(Avg=Avg('Sale'))
        }
        r3 = {
            'Name': i,
            'Year': d[4].Year + "-" + d[0].Year,
            'Result': d.exclude(Sale=0).aggregate(Avg=Avg('Sale'))
        }
        r4 = {
            'Name': i,
            'Year': e[4].Year + "-" + e[0].Year,
            'Result': e.exclude(Sale=0).aggregate(Avg=Avg('Sale'))
        }
        result.append(r1)
        result.append(r2)
        result.append(r3)
        result.append(r4)
    return render(request, 'ProductSale.html', {'context': result})


def least_sale(request):
    data()
    result = []
    for i in Product.objects.all():
        a = Report.objects.filter(ProductName=i)
        r = {
            'Name': i,
            'Year': a.exclude(Sale=0).order_by('Sale')[0].Year,
            'Sale': a.exclude(Sale=0).order_by('Sale')[0].Sale,
        }
        result.append(r)
    return render(request, 'LeastSale.html', {'context': result})
