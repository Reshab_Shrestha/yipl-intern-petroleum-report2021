from django.contrib import admin

# Register your models here.
from Report.models import Product, Report, Country

admin.site.register(Product)
admin.site.register(Report)
admin.site.register(Country)
