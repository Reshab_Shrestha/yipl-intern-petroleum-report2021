from django.test import SimpleTestCase
from django.urls import reverse, resolve

from Report.views import sale_product, sale_country, least_sale


class TestUrls(SimpleTestCase):
    def test_country_url_is_resolved(self):
        url = reverse('countrysale')
        self.assertEquals(resolve(url).func, sale_country)

    def test_product_url_is_resolved(self):
        url = reverse('productsale')
        self.assertEquals(resolve(url).func, sale_product)

    def test_least_url_is_resolved(self):
        url = reverse('leastsale')
        self.assertEquals(resolve(url).func, least_sale)