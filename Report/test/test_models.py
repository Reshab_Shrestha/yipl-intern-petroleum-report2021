from Report.models import Product, Report, Country


def setUp(self):
    self.proj = Product.objects.create(ProductName='Hello')


def setUp(self):
    self.cou = Country.objects.create(CountryName='Nepal')


def test_Report_created(self):
    self.proj1 = Report.objects.create(
        ProductName=self.proj,
        Year='2020',
        Sale='1234',
        CountryName=self.cou
    )
    self.assertEquals(str(self.proj1), "Hello 2020 1234 Nepal")


def test_Product_created(self):
    self.proj2 = Product.objects.create(ProductName='Hello')
    self.assertEquals(str(self.proj2), "Hello")


def test_Country_created(self):
    self.cou1 = Country.objects.create(CountryName='Nepal')
    self.assertEquals(str(self.proj2), "Nepal")