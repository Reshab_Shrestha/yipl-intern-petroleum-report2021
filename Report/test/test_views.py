from django.test import TestCase, Client
from django.urls import reverse, resolve


class TestViews(TestCase):
    def test_productsale(self):
        client = Client()
        response = client.get(reverse('productsale'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'ProductSale.html')

    def test_Countrysale(self):
        client = Client()
        response = client.get(reverse('countrysale'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'CountrySale.html')

    def test_leastsale(self):
        client = Client()
        response = client.get(reverse('leastsale'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'LeastSale.html')
